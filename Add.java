public class Add {
	
	public static void main(String[] args) {
		Add add = new Add();
		System.out.println("Output of n1+n2 = "+add.add(5, 6));
	}
	
	private int add(int n1, int n2) {
		return n1+n2;
	}
	
	private int add(int n1, int n2, int n3) {
		System.out.println("returning n1+n2+n3");
		return n1+n2+n3;
	}

}
